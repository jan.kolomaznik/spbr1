package jm.test.model;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Nákupní košík.
 * Uchová informace o produktech a objednaném množstí.
 */
public class Basket implements Iterable<Map.Entry<Product, Integer>> {

    private Map<Product, Integer> products;

    /**
     * Konstruktor pro vytvoření prázdného košíku.
     */
    public Basket() {
        this.products = new LinkedHashMap<>();
    }

    /**
     * Metoda pro získání seznamu produktů v kočíku.
     * @return Kolekci produktů uchovávajíci pořadí, v jakém byli produkty do košíku vloženy.
     */
    public Collection<Product> getProducts() {
        return products.keySet();
    }

    /**
     * Ziskáni množstí jednoho produktu v košíku.
     * @param product produl
     * @return amount of product
     * @throws ShopException if product si not in basket;
     */
    public Integer getAmountOfProduct(Product product) {
        if (!products.containsKey(product)) {
            throw new ShopException("Product not in basket");
        }
        Integer amount = products.get(product);
        assert amount > 0 : "Amount of product have to by positive number greater then zero.";
        return amount;
    }

    /**
     * Metoda pro přidání produktu do košíku.
     * @param product konkrétní produkt
     * @param amount množství objednaného produktu.
     */
    public void addProduct(Product product, Integer amount) {
        if (amount < 1) {
            throw new ShopException("Amount of product must be positive number");
        }
        products.put(product, amount);
    }

    @Override
    public Iterator<Map.Entry<Product, Integer>> iterator() {
        return products.entrySet().iterator();
    }

    public Stream<Map.Entry<Product, Integer>> stream() {
        return StreamSupport.stream(products.entrySet().spliterator(), false);
    }
}
