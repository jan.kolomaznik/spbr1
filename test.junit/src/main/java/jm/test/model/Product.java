package jm.test.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Třída pro produkt.
 * Každý produkt je jednoznačně identifikován svým jménem, které se nemění.
 * Produkt má také cenu, kterou je možné modifikovat.
 */
public class Product {

    private final String name;

    private BigDecimal prise;

    public Product(String name, BigDecimal prise) {
        this.name = name;
        this.prise = prise;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrise() {
        return prise;
    }

    public void setPrise(BigDecimal prise) {
        this.prise = prise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", prise=" + prise +
                '}';
    }
}
