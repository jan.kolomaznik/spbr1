package jm.test.model;

import java.math.BigDecimal;

/**
 * Rozhraní pro objednávávací systém internetového obchodu.
 */
public interface OrderService {

    /**
     * Metoda pro vytvoření objednávky z nákupního košíku.
     *
     * Objednávka se zkládá z následující položek:
     * 1. Uživatle, který se přebírá z nákupního košíku.
     *
     * @param user
     * @param basket
     * @return created Order
     */
    Order createOrder(User user, Basket basket);

    /**
     * Tato metody vypočítá, jestli objednávka již spadá do prioritních objednávek nebo ještě ne.
     *
     * Podminky jsou:
     * <ul>
     *     <li>Uživatel je členem klubu s předplacenám členctvím</li>
     *     <li>Velikost objednáky je přes 1000,-</li>
     * </ul>
     * @param user informace o uživateli
     * @param basket objednávka
     * @return category
     */
    Order.Category establishCategory(User user, Basket basket);
}
