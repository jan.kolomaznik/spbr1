= Spring: Dependency Injections
Jan Kolomazník <jan.kolomazník@gmail.com>
v1.1, 2020-06-30
include::../header.adoc[]

== Definice
Vkládání závislostí (anglicky Dependency injection (DI)) je v objektově orientovaném programování technika pro vkládání závislostí mezi jednotlivými komponentami programu tak, aby jedna komponenta mohla používat druhou, aniž by na ni měla v době sestavování programu referenci.
Dependency injection lze chápat jako novější název pro Inversion of Control (IoC), ale v užším významu.[zdroj?] Jedná se o konkrétní techniku IoC footnote:[Zdroj: https://cs.wikipedia.org/wiki/Vkl%C3%A1d%C3%A1n%C3%AD_z%C3%A1vislost%C3%AD[Wikipedia]].

=== Pevným vazbám se snažíme vyhýbat!

[source,java]
----
public interface CarDao {}

public class CarDaoImpl implements CarDao {}

// zde mezi CarDao a CarService je pevná vazba
public class CarServiceImpl {
    private CarDao carDao = new CarDaoImpl();
}
----

*Proč?*
- Vaše architektura je příliš úzce svázaná (podobně jako u dědičnosti) a málo flexibilní.
- Změna kódu může být velice obtížná.
- Třída bez pevných vazeb se lépe testuje.

*Jak?*
- Princip IoC přesouvá odpovědnost za vznik vazeb na někoho jiného.
- V našem případě je přesunut z programátora na framework.

== Inversion of Control

Technika pro vkládání závislostí mezi jednotlivými komponentami programu.
Vytváří vazby mezi komponenty místo programátora. 

==== Mezi výhody patří:

* _Lepší přehlednost kódu_ – na první pohled jsou zřejmé závislosti.
* _Jednodušší struktura aplikace_ – uplatňování principů IoC vede k vytváření opakovaně použitelných komponent.
* _Opakovaná použitelnost_ – třídy (komponenty) využívající DI pro nastavení závislostí jsou snadněji opakovaně použitelné v jiných prostředích či aplikacích.
* _Lepší testovatelnost_ – třídy se dají lépe testovat a snadno mockovat.

==== Nevýhody (rizika) jsou:

* Pro vývojáře, který není s principy IoC obeznámen, může být zpočátku složitější se v takto napsaném kódu orientovat.
* Využití DI kontejneru může mít vliv na rychlost aplikace, zvlášť v prostředích nepodporujících vícevláknové zpracování.

== Dependency injection (DI)

Komponenty jsou odstíněny od této správy vazeb (závislostí), tuto odpovědnost přebírá *dependency provider* neboli context. 

DI zahrnuje tedy nejméně tři objekty, které spolu musí spolupracovat. 

* *Konzument*, tedy objekt požadující služby.
* *Poskytovatele* těchto služeb mu dodá DI provider, který ve skutečnosti zodpovídá za celý životní cyklus poskytovatele.
* *Provider* neboli injector může být implementován několika způsoby, např. jako lokátor služeb, abstraktní továrna, tovární metoda, nebo pomocí nějakého z řady z frameworků, například Spring.

https://martinfowler.com/[Martin Fowler] poukazuje na tři různé vzory, jak lze objektu přidat externí referenci jiného objektu.

. *Vkládání rozhraním* – externí modul, který je do objektu přidán, implementuje rozhraní, jež objekt očekává v době sestavení programu.
. *Vkládání setter metodou* – objekt má setter metodu, pomocí níž lze závislost injektovat.
. *Injekce konstruktorem* – závislost je do objektu injektována v parametru konstruktoru již při jeho zrodu.

=== Dependency injection se Spring Framework
Spring v základu vznikl jeko DI Framework.

Spring podporuje všechny tři typy *injekce* a nekolik způsobů *konfigurace*.
Pro přehlednost a větší flexibilitu je dobré mít oddělenu konfiguraci od implementace. 

. *Existují tři typy injekce a označuje je jako*:
* Property inject
* Constructor inject
* Setter inject
. *Spring podporuje několik způsobů konfigurace*:
* XML (legacy)
* Anotacemi
* Definicí v kodu (metodou)

Máme tedy celkem 9 možností jak to udělat.
Kterou ale použít:

- *Pro bysnis logiku*: nejčastěji používáme _Property inject_ nebo _Constructor inject_ pomocí anotací.

- *Pro konfiguraci aplikace*: se požívajájí _definice v kódu_ a nco co bych nazval _Parametr inject_.

Aby Spring mohl do objektu nastavit zásisloti.
Musí o bou objektech "vědet", to znamenná, že oba objekty musí existovat k kontextu Spring Framorku, zjednodušene řešeno musí jím být oba vytvořeny.

Jedním ze způsobů je označit třídu annotaci `@Component` nebo nebo nějakou z odvozených anotaci.

[WARNING]
SpringBoot ale hledá takové komponenty pouze v _podbalíčcíci_ spouštěcího objektu označeného anotací '@SpringBootApplication'.
Pro jednoduchot tuto třídu nadále označujme jako _hlavní třídu_.


.Příklad použítí anotace `@Component`
[source,java,indent=0]
----
include::src/main/java/jm/spring/config/CarDao.java[tag=CarDao]
----

Takto je vytvořen v kontextu Spring objekty typu CarDao.
Tomu tojektu je přiřazeno jedinečné jméno: _CarDao_.

Další možností je vytvoření a konfigurace objektu pomocí metody.
Tato taková metoda se může nacházet například v _hlavní třídě_ aplikace.
A metoda musí být označena anotací `@Bean`.

.Příklad definice pomocí anotace `@Bean`
[source,java,indent=0]
----
include::src/main/java/jm/spring/config/DIApplication.java[tag=vipCarDao]
----

Vysldkem jsou dva různé objekty stejné třídy s různými mény v kontext.
Těmto objektům se v terminologii SpringFrameworku říká *Bean*.

====
.Otestováí Constructro Injection
Funkčno výše zmíněných definice je demonstorána v testu ve tříde link:src/test/java/jm/spring/config/DIApplicationTests.java[DIApplicatioTests].

[source,java,indent=0]
----
include::src/test/java/jm/spring/config/DIApplicationTests.java[tag=constructorInjectService]
----
====
=== Property inject pomocí annotací
Tato se nastaveje označením aiributu třídy anotaci `@Autowired`.
Framework si sám najde danou property pomocí reflexe.

.Příklad injekce pomocí atributu
[source,java]
----
include::src/main/java/jm/spring/config/PropertyInjectService.java[tag=PropertyInjectService]
----
<1> Povinná anotace
<2> Typ a jmeno doplněnné beany, pokud je v kontextu jen jedna beana požadovaného typu, jméno se nepoužije.

==== Jaké jsou výhody a nevýhody?
- _Minimum kódu_
- _Závislost na Spring_
- _Obtížneji testovatelné_
- _Možnost kruhových závislostí_

====
.Otestování property Injekce
Funkčnost výše zmíněných definice je demonstorána v testu ve tříde link:src/test/java/jm/spring/config/DIApplicationTests.java[DIApplicatioTests].

[source,java,indent=0]
----
include::src/test/java/jm/spring/config/DIApplicationTests.java[tag=propertyInjectService]
----
<1> Test že dependenci byla nastavene pomocí property.
====

[TIP]
====
.Závislost na více objektech stejné třídy

Nekdy může chtít načíst všechny _Beany_ určitéyo typu, V takovém případě stačí u definice použit pole nebo nějakou kolekci.

[source,java,indent=0]
----
include::src/test/java/jm/spring/config/DIApplicationTests.java[tag=autowiredArray]
----
<1> Otestováín že pole Dao bylo naplněno dvěma referencemi.
    Na Beanu `carDao` a `vipCarDao`.
====

=== Constructor inject
Při vytváření se používá konstruktor instance s argumenty.

.Příklad injekce pomocí konstruktrou
[source,java]
----
include::src/main/java/jm/spring/config/ConstructorInjectService.java[tag=ConstructorInjectService]
----
<1> Atributy třídy jsou bez anotace a mohou být `final`.
<2> Anotace `@Autowired` *není* povinná.
<3> Závisloti se předávají přes kontruktor.

==== Jaké jsou výhody a nevýhody?
- _Objektově čistý kódu_
- _Bez závislosti na Spring_
- _Méně přehledné a huře udržovatelné_
- _Snadno testovatelné_
- _Nesnese kruhové závislosti_

====
.Otestováníí Constructro Injection
Funkčno výše zmíněných definice je demonstorána v testu ve tříde link:src/test/java/jm/spring/config/DIApplicationTests.java[DIApplicatioTests].

[source,java,indent=0]
----
include::src/test/java/jm/spring/config/DIApplicationTests.java[tag=constructorInjectService]
----
<1> Test že dependenci byla nastavene pomocí konstruktoru.
====

[TIP]
====
.Použití link:https://projectlombok.org/[projectu lombok].
Vše je možné zjednodušit použitím annotace `@RequiredArgsConstructor`, která za nás konstruktor do třídy doplní.

[source,java]
----
@Component
@RequiredArgsConstructor
public class ConstructorInjectService {

    private final CarDao carDao;
}
----

V lomboku existuje ještě anotace `@AllArgsConstructor` ta však není vhodná pokud střída obsahuje krom závislostí i configurace.
====

=== Další možnosti injekce
V následujím textu jsou uvedeny další možnosti.
Ty zde uvádím spíše do počtu, přestože i pro ně se dá v jistých případech najít praktické využití.

==== Setter inject
Při vytváření je nahrána instance pomocí setter-u.

[source,java]
----
private CarDao carDao;

@Autowired
public void setCarDao(CarDao carDao) {
    this.carDao = carDao;
}
----

==== Java konfigurace
Konfigurace Bean ve speciální třídě.
Pak je možnost nastavit závislosti přes parametry nebo volání příslušných _Bean_ metod.

[source,java]
----
@Configuration
public class ContextConfig {

    // vytvoří beanu typu CarDao a názvem carRepository
    @Bean
    public CarDao carRepository() {
            return new CarDaoImpl();
    }

    // vytvoří beanu CarService a injektuje ji CarDao (CarRepository)
    @Bean
    public CarService carService(CarDao carDao) {
            return new CarServiceImpl(carDao);
    }
}
----

==== XML konfigurace
Je reprezentována XML souborem.

[source,xml]
----
<bean id="..." class="..."> vytvoří beanu
<import resource="..."/> import jiné konfigurace
<context:component-scan base-package="..." /> skenování package
----

[source,xml]
----
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="..." class="...">
        <!-- collaborators and configuration for this bean go here -->
    </bean>

    <bean id="..." class="...">
        <!-- collaborators and configuration for this bean go here -->
    </bean>

    <!-- more bean definitions go here -->

</beans>
----

== Spring: základní pojmy

Zde je nutné znát dva pojmy:

=== Bean

Objekt, který vykonává nějakou funkčnost (např. přidává data do databáze, vyhledává…). 

Beany žijí v kontejneru (context). 

Lze s ním pracovat v celé aplikaci. 

Existují dva typy bean (scope):

- *Singleton* objekt je v celé aplikaci jen jednou.
 Pokaždé, pokud si řekneme o daný objekt aplikačnímu kontextu, dostaneme stejnou instanci.
- *Prototype* je podobný jako singleton.
 Rozdíl je v tom, že pokud si řekneme o daný objekt aplikačnímu kontextu, dostaneme vždy novou instanci.

=== Context

Jedná se kontejner, neboli aplikačním kontextu, ve kterém objekty (Bean), které tvoří funkční jádro vaší aplikace. 

Context se zavádí při startu aplikace a reprezentuje ho třída `ApplicationContext`. 

V celé aplikaci je jen jeden a dá se injektovat odkudkoli.

*ApplicationContext* poskytuje:

- Bean factory metody pro přístup k aplikačním komponentám.
- Schopnost načíst prostředky souboru obecným způsobem.
- Možnost publikovat události registrovaným posluchačům.
- Schopnost řešit zprávy, podporovat internacionalizaci.
- Dědičnost z rodičovského kontextu.
 Definice v potomku kontextu budou mít vždy přednost. 

=== Základní annotace

Je realizována obyčejnou Java třídou, ve které jsou použity anotace pro tvorbu aplikačního kontextu. 

Je dobré, si pro konfigurační třídy udělat speciální package (configuration) nebo je umistit do "základního" balíčku.

* *`@Configuration`* vytvoří z dané třídy konfigurační třídu
* *`@Import`* spojí dvě konfigurace (naimportuje jinou konfiguraci)
* *`@Bean`* vytvoří beanu; typ je návratová hodnota a název je název metody (pokud se nepoužije name).
 Lze i změnit defaultní singleton scope (`scope=DefaultSco­pes.PROTOTYPE`)
* *`@Autowired`* injektuje instanci jiné beany
* *`@ComponentScan`* - proskenuje zadané package.
 Pokud narazí na speciální anotace:
* *`@Component`*: Tato annotace se nepoužívá, slouží jako "předek" pro následující komponenty:
- `@Controller`: prezentační vrstva;
- `@Service`: aplikační vrstva;
- `@Repository`: datová vrstva) vytvoří z daných tříd beany.

