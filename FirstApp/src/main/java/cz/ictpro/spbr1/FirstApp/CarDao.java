package cz.ictpro.spbr1.FirstApp;

import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ToString
public class CarDao {
    private final String name;

    @Autowired(required=false)
    private PersonService personService;

    public CarDao(String name) {
        this.name = name;
    }

    public CarDao() {
        this.name = "carDao";
    }
}
