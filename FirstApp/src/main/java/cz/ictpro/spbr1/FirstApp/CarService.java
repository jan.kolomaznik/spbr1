package cz.ictpro.spbr1.FirstApp;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

/** nejsilnejci */
public class CarService extends CarDao {

    /** Neslabsi */
    public void use(CarDao carDao) {}

    /** Agregace */
    private CarDao carDao;
    public CarService(CarDao carDao) {
        this.carDao = carDao;
    }
    public void agregace() {}

    /** Kompozice */
    public CarService() {
        this.carDao = new CarDao();
    }
    public void kompozice() {}

    @RequiredArgsConstructor
    static class LombokAgregace {
        private final CarDao carDao;
    }

    static class AutowiredAgregace {
        @Autowired
        private CarDao carDao;
    }
}
