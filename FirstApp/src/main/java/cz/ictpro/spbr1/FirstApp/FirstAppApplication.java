package cz.ictpro.spbr1.FirstApp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Arrays;
import java.util.Objects;

@SpringBootApplication
public class FirstAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstAppApplication.class, args);
	}

/*
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext context) {
		return args -> {
			for (var name: context.getBeanDefinitionNames()) {
				System.out.println(name);
			}
		};
	}

/*
	@Bean
	public CommandLineRunner commandLineRunner(CarDao[] carDaos) {
		return args -> {
			for (var carDao: carDaos) {
				System.out.println(carDao);
			}
		};
	}

	*/

	@Bean
	public CommandLineRunner commandLineRunner(CarDao vipCarDao) {
		return args -> {
			System.out.println(Objects.equals(vipCarDao(), vipCarDao()));
		};
	}



	@Bean
	@Scope("prototype")
	public CarDao vipCarDao() {
		return new CarDao("vipCarDao");
	}



}
