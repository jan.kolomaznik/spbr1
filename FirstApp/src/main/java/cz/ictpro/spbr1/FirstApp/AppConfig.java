package cz.ictpro.spbr1.FirstApp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean(name="#{demoService}")
    public PersonService service() {
        return new PersonService();
    }
}

