package cz.ictpro.spbr1.FirstApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class FirstAppApplicationTests {

	@Autowired
	private CommandLineRunner commandLineRunner;


	@Test
	void contextLoads() throws Exception {
		commandLineRunner.run();
	}

}
