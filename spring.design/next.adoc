* link:../spring.cloud/readme.adoc[]
* *Prezentační vrstva*:
  - link:../spring.web/readme.adoc[Web]
  - link:../spring.shell/readme.adoc[]
* *Business layer*:
  - link:../spring.security/readme.adoc[Security]
  - link:../spring.messaging/readme.adoc[]
* *Data layer*:
  - link:../spring.data.jdbc/readme.adoc[JDBC Template]
  - link:../spring.data.jpa/readme.adoc[Java Persistence API]
