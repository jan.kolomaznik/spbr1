= Spring: Design aplikací
Jan Kolomazník <jan.kolomazník@gmail.com>
v1.1, 2020-07-03
include::../header.adoc[]

== Úvod
Spring podporuje množství způsobů jak navrhovat strukturu celé aplikace.
Můžeme aplikaci postavit jako monolit, ale také ji postavit na micro-servisní architektuře.

Součástí je designu je taký určení i způsobu nasazení aplikace.
Spring podporuje psaní služeb, které se spouštějí jako samostatné aplikace nebo je možné jej nasadit do Servlet kontejneru (Tomcat) ale také do Docker kontejneru a do různých cloudu.

Dalšími rozhodnutími je jaké rozhraní se budou implementovat,
Jestli bude mít aplikace webové rozhraní nebo API ve formě REST nebo SOAP.
Další možností je komunikace s dalšími systém například pomocí Message Brokeru.

Ať již se rozhodneme pro monolitickou nebo mikroservisicvou architekturu.
Základem obou je třívrstvá architektura.

== Třívrstvá architektura
Spring podporuje takzvanou třívrstvou architekturu, která rozkládá aplikaci do tří úrovní:

image:media/3-tier-architechture.png[]

=== Prezentační vrstva
V rámci Springu existuje celá paleta modulů, které nám pomáhají realizovat tuto vrstvu.
Rovněž existuje řada technologií, které mohou snadno implementovat konkrétní rozhraní.
Mezi zajímavé možnosti patří:

- *Spring web*: pro tvorbu webového rozhraní a REST API
- *Spring shell*: pro tvorbu CLI rozhraní
- *Spring actuator*: je knihovna pro generování webového rozhraní s metrikami.
- *Spring flux*: altrnativa k Spring web.

=== Aplikační vrstva
V této vrstvě se primárně implementuje aplikační logika.
Spring framework má některé podpůrné knihovny, které ale často zasahují i do prezentační a datové vrstvu.
Proto bývá často obtížné určit kam tyto moduly patří.

- *Spring security*: Pro zabezpečení aplikace.
- *Spring messaging*: Pro zasílání zpráv mezi komponentami.

=== Datová vrstva
Datová vrstva může být rovněž implementována různě.
Základem je napojení na různé databáze.
Spring umožňuje pracovat s SQL databázemi přes více rozhraní.
Spring rovněž podporuje práce s noSQL databází.

- *Spring data jdbc*: pro práci s databázemi přes jdbc template
- *Spring data jpa*: pro práci pomocí ORM mapování.
- *Spring data mongodb*: pro práci nosql mongodb databází
- *Spring data neo4j*" pro práci s grafovou databází.

== Microservice architektura
Spring Cloud se zaměřuje na podporu Microservices jako další úrovně aplikace.
Každá microservice je defakto malá Spring Boot aplikace, která ale je do značné míry nezávislá na okolních Microservices a která nese uzavřený funkční celek aplikace.
Tato nezávislost nám umožňuje, ay Microservice byli jednodušší, ale přináší větší nároky na konfiguraci jako celku.

Například pokud chceme analyzovat logy nebo sbírat metriky musíme tyty data agregovat ze všech service.
Pokud řešíme autentizaci, můžeme vytvořit nezávislý autentizační  službu, se kterou budou všechny service komunikovat. Pokud chceme aby se service školovali podle nároků uživatelů (spouštěli další když jedna nebude stačit) případně aby se se sami nahazovali další když jedna zamrzne, pak musíme servisy registrovat.
Obdobně s konfiguraci, tu již nemůžeme mít v souborech, ale musíme si je vyžadovat z centrálního úložiště ...

image::media/diagram-microservices.svg[]

Jak je vidět, moduly Spring Cloud nám pomáhají vyřešit většinu těchto problémů.

=== API First vs Code First API Development
Pokud jde design API služby, existují dva důležité myšlenkové směry:

1. přístup „API First“
2. přístup „Code First“

Přístup Code First je tradičnějším přístupem k vytváření API, přičemž vývoj kódu probíhá po rozložení obchodních požadavků, případně generování dokumentace z kódu.

Přístup Design First se zasazuje o to, aby místo kódu se začalo návrhem API.

image::media/designvscode.jpg[]

Při vývoji mikroservice s REST rohraním, je zvolení přístupu **API First** lepší.
Jde o stejné důvody jako v případe návrhu rozhraní objektů u OOP.

Další výhody tohoto přástupu jsou popsány na bloku https://swagger.io/blog/api-design/design-first-or-code-first-api-development/[Design First or Code First: What’s the Best Approach to API Development?].