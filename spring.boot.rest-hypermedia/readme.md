[remark]:<class>(center, middle)
# Spring Boot
## REST Hypermedia

[remark]:<slide>(new)
## Úvod
Hypermedia je jedním z REST přístupů.
 
Umožňuje ještě více oddělit klienty od služby.
 
REST odpovědi nebosahují jen data, ale i odkazy na související zdroje. 

Client tedy nemusí mít informace jako:
- Jak bytvořit HTTP adresu zdroje z id.
- Jaká je struktura REST rozhranní.
- Jaké adresy REST zpřístupňje.

Více informací o tomto konceptu [HATEOS](https://en.wikipedia.org/wiki/HATEOAS)

[remark]:<slide>(new)
### Zadání
Vytvoříme službu, která  bude přijímat `HTTP GET` na adrese

```
http://localhost:8080/greeting
```

a vygeneruje zprávu v JSON formátu:

```json
{
  "content":"Hello, World!",
  "_links":{
    "self":{
      "href":"http://localhost:8080/greeting?name=World"
    }
  }
}
```

Pozdrav budé možné paralelizovat pomocí parametru `name`, tak že výsledná odpověď pak bude:

```json
{
  "content":"Hello, User!",
  "_links":{
    "self":{
      "href":"http://localhost:8080/greeting?name=User"
    }
  }
}
```

[remark]:<slide>(new)
## Gradle konfigurace
Pro konfiguraci projektu použijem [Gradle](https://gradle.org/) script. 

Podobně je ale možno použít i [Maven](https://maven.apache.org/).

Ve sriptu použijeme následující `dependencies`:

#### `build.gradle`
```groovy
dependencies {
    compile("org.springframework.boot:spring-boot-starter-hateoas")
}
```

[remark]:<slide>(new)
## Vytvořte třídu pro resource
NYní vytvoříme třídu reprezentující odpověď na požadavek.

Odpoveď ve formátu JSON bude obsahovat:
- data uložená v `content`
- link na samotný zdroj

Nejpodstatnější zde je odkaz odkazující na samotný zdroj: 

```json
{
  "content":"Hello, World!",
  "_links":{
    "self":{
      "href":"http://localhost:8080/greeting?name=World"
    }
  }
}
```

Element `_links` může obsahovat senzam odkazů
- ty jsou definovány jménem a linekm
- seznam by měl obsahovat `self` link.

[remark]:<slide>(new)
### Třída `ResourceSupport`
Třída `ResourceSupport` umožňuje přidávat `link` do odpovědí a zajišťuje jejich zprávné přegenerování do JSON.
 
#### `src/main/java/hello/Greeting.java`
```java
package hello;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Greeting extends ResourceSupport {

    private final String content;

    @JsonCreator
    public Greeting(@JsonProperty("content") String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
```

#### Notes
`@JsonCreator` - annotace pro Jackson definující konstruktor pro vytvoření objektu z JSON
`@JsonProperty` - mapování property na parametry.

[remark]:<slide>(new)
## Vytvořte regulátor prostředků
Pro zpravocání požadavků Vytvoříme třídu `GreetingController`.

#### `src/main/java/hello/GreetingController.java`
```java
package hello;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {

    private static final String TEMPLATE = "Hello, %s!";

    @RequestMapping("/greeting")
    public HttpEntity<Greeting> greeting(
            @RequestParam(value = "name", defaultValue = "World") String name) {

        Greeting greeting = new Greeting(String.format(TEMPLATE, name));
        greeting.add(linkTo(methodOn(GreetingController.class).greeting(name)).withSelfRel());

        return new ResponseEntity<>(greeting, HttpStatus.OK);
    }
}
```

[remark]:<slide>(new)
### Rozbor třídy `GreetingController`
Nejzajímavější mistem je vytvoření odkazu. 

Pro sestavení `link` oobjektu je použita metoda  `linkTo(...)` a `methodOn(...)` z třídy `ControllerLinkBuilder`.

Ty umožňují falešně vyvolat metodu na kontoleru a tím definovat link. 

Vrácená položka LinkBuilder bude kontrolovat anotaci mapování metody řadiče, aby vytvořila přesně URI, na který je metoda mapována.

Volánim metody `withSelfRel()` vytvoříme self link pro odpověď.


[remark]:<slide>(new)
## Vytvoření spustitelné aplikace
Pro spuštění aplikace můžeme vytvořit jednoduchou spouštěcí třídu s metodou `main`. 

#### `src/main/java/hello/Application.java`
```java
package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

Pokud používáte Gradle, můžete spustit aplikaci pomocí `./gradlew bootRun`

[remark]:<slide>(new)
## Testování aplikace
Po zadání url `http://localhost:8080/greeting` do prohlížeče, měli by jste získat nasleující výtstup:

```json
{
  "content":"Hello, World!",
  "_links":{
    "self":{
      "href":"http://localhost:8080/greeting?name=World"
    }
  }
}
```

Pokud do uml nastavíme paramter `name`: `http://localhost:8080/greeting?name=User`, odpověď se změní následovně:

```json
{
  "content":"Hello, User!",
  "_links":{
    "self":{
      "href":"http://localhost:8080/greeting?name=User"
    }
  }
}
```

[remark]:<slide>(new)
## Rozšíření:
V REST není dobré odesílat objekt přímo, je jepší jej "zabalit" a přidat k nemu metadata, například čas vytvoření.
- Upravte odpověď títo způsobem

Vytvořete v Controleru alernativní metody pro jiné jazyky a spolu s odpovědí, odšlete i link na ně.
