package com.example.demo;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class Response<T> {

    private final T content;

    private LocalDateTime created = LocalDateTime.now();

}
