package com.example.demo.person;

import org.springframework.stereotype.Service;

import java.util.*;

@Service
class PersonService {

    private Map<UUID, Person> cache = new HashMap<>();

    PersonService() {
        var id = UUID.fromString("3de482b0-8920-4b96-8078-85287939c292");
        var person = new Person();
        person.setId(id);
        person.setName("Pepa");
        person.setAge(15);
        person.getRoles().add("ADMIN");
        this.cache.put(id, person);
    }

    Collection<Person> findAll() {
        return cache.values();
    }

    Optional<Person> findById(UUID id) {
        return Optional.ofNullable(cache.get(id));
    }

    Person create(String name, int age) {
        var p = new Person();
        p.setId(UUID.randomUUID());
        p.setName(name);
        p.setAge(age);
        p.getRoles().add("HOST");
        cache.put(p.getId(), p);
        return p;
    }

}
