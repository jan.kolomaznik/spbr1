package com.example.demo.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
public class Person {

    private UUID id;

    private String name;

    private int age;

    //@JsonIgnore
    private Set<String> roles = new HashSet<>();
}
