package com.example.demo.person;

import com.example.demo.NotFoundException;
import com.example.demo.Response;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api")
class PersonController {

    @Autowired
    private PersonService personService;


    @GetMapping(value = "/person", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    Collection<Person> getPersons() {
        return personService.findAll();
    }


    @GetMapping("/person/{id}")
    @ResponseStatus(HttpStatus.OK)
    Response<Person> getPerson(@PathVariable UUID id) {
        return personService
                .findById(id)
                .map(Response::of)
                .orElseThrow(NotFoundException::new);
    }

    @Data
    static class RequestPerson {
        @Size(min = 5, max = 32)
        private String name;

        @Min(0)
        private int age;
    }

    @PostMapping("/person")
    @ResponseStatus(HttpStatus.CREATED)
    Person postPerson(@RequestBody @Validated RequestPerson person) {
        return personService.create(
                person.name,
                person.age
        );
    }

    /*
    @PutMapping("/person/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)

    @DeleteMapping("/person/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
*/

}
