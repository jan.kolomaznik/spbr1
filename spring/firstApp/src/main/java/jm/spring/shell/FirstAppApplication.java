package jm.spring.shell;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

// tag::SpringBootApplication[]
@SpringBootApplication //<1>

public class FirstAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(FirstAppApplication.class, args); //<2>
    }
	// end::SpringBootApplication[]

    // tag::commandLineRunner[]
    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> System.out.println("Hello from Spring.");
    }
    // end::commandLineRunner[]
}
