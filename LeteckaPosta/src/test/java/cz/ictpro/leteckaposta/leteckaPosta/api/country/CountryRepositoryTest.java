package cz.ictpro.leteckaposta.leteckaPosta.api.country;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@SpringBootTest
class CountryRepositoryTest {

    @Autowired
    private CountryRepository countryRepository;

    @Test
    void getCountries() {
        var countires = countryRepository.findAll();
        Assertions.assertEquals(2, StreamSupport.stream(countires.spliterator(), false).count());
    }
}