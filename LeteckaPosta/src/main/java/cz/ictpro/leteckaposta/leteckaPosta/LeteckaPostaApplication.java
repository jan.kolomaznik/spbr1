package cz.ictpro.leteckaposta.leteckaPosta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@RestController
public class LeteckaPostaApplication {

    public static void main(String[] args) {

        SpringApplication.run(LeteckaPostaApplication.class, args);
    }

    /*
    @RequestMapping(path = "/**")
    void cokoliv(HttpServletRequest request) {
        System.out.println(request);
    }
     */

}
