package cz.ictpro.leteckaposta.leteckaPosta.api.country.capital;

import javax.persistence.Embeddable;

@Embeddable
public class LatLon {

    private Long lat;
    private Long lon;

}
