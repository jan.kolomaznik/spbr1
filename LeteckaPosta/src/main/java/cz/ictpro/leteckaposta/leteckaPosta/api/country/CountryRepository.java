package cz.ictpro.leteckaposta.leteckaPosta.api.country;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
interface CountryRepository extends PagingAndSortingRepository<Country, UUID> {

    Optional<Country> findByCode(String code);

}
