package cz.ictpro.leteckaposta.leteckaPosta.utils;

import lombok.Builder;
import lombok.Getter;
import lombok.Value;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
public class ResponsePage<R extends Object> {

    public static <P> ResponsePage<P> of(List<P> data, int page, int size, int pages) {
        var result = new ResponsePage<P>();
        result.data = data;
        result.page = new Page(page, size, pages);
        return result;
    }

    @Value
    private static class Page {

        private int page;
        private int size;
        private int pages;

    }

    private List<R> data;

    private Map<String, Object> metaData = new LinkedHashMap<>();

    private Page page;


}
