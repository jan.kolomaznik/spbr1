package cz.ictpro.leteckaposta.leteckaPosta.api.country;

import cz.ictpro.leteckaposta.leteckaPosta.utils.NotFoundException;
import cz.ictpro.leteckaposta.leteckaPosta.utils.ResponsePage;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@RestController
@RequestMapping(path = "${api.path}/country")
@Slf4j
class CountryController {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CountryService countryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    ResponsePage<Country> getCountries(Pageable pageable) {
        var page = countryRepository.findAll(pageable);
        return ResponsePage.of(page.toList(), page.getNumber(), page.getSize(), page.getNumberOfElements());
    }

    @GetMapping(path = "/{id:[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}}")
    @ResponseStatus(HttpStatus.OK)
    Country getCountryById(@PathVariable UUID id) {
        log.info("Get Country by id:{}", id);
        return countryRepository
                .findById(id)
                .orElseThrow(NotFoundException::new);
    }

    @GetMapping(path = "/{code:[A-Z]+}")
    @ResponseStatus(HttpStatus.OK)
    Country getCountryByCode(@PathVariable String code) {
        log.info("Get Country by code:{}", code);
        return countryRepository
                .findByCode(code)
                .orElseThrow(NotFoundException::new);
    }

    @Setter
    static final class CreateCountryRequest {
        @Length(min = 2, max = 3)
        String code;
        @NotBlank
        String capital;
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    Country createCountry(@RequestBody @Valid CreateCountryRequest request) {
        log.info("Create country {}", request);
        return countryService.createWithCapital(
                request.code,
                request.capital
        );
    }

}

