package cz.ictpro.leteckaposta.leteckaPosta.api.country;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class CountryShell {

    @Autowired
    private CountryService countryService;

    @ShellMethod
    String createCountry(String code, String capital) {
        return countryService.createWithCapital(code, capital).toString();
    }
}
