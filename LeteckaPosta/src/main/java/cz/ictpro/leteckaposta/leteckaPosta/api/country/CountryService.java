package cz.ictpro.leteckaposta.leteckaPosta.api.country;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
class CountryService {

    @Autowired
    private CountryRepository countryRepository;

    /**
     * To store new country Entity in database together with his single main city.
     */
    Country createWithCapital(
            String code,
            String capital
    ) {
        var country = new Country();
        country.setCode(code);
        // TODO napleni a save
        return countryRepository.save(country);

    }

}
