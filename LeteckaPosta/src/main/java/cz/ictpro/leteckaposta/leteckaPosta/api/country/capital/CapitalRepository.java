package cz.ictpro.leteckaposta.leteckaPosta.api.country.capital;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface CapitalRepository extends PagingAndSortingRepository<Capital, UUID> {

    Stream<Capital> findByName(String name);
}
