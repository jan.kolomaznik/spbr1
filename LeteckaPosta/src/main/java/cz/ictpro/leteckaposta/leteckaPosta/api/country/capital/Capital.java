package cz.ictpro.leteckaposta.leteckaPosta.api.country.capital;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.ictpro.leteckaposta.leteckaPosta.api.country.Country;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Data
@Entity
@Table(name = "capitals")
public class Capital implements Serializable {

    @Id
    @Column(name = "capital_id")
    @EqualsAndHashCode.Exclude
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    @ToString.Exclude
    @JsonIgnore
    private Country country;

    private String name;

    //@Embedded
    //private LatLon latLon;

}
