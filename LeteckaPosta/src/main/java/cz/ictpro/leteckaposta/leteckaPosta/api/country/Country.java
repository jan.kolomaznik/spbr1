package cz.ictpro.leteckaposta.leteckaPosta.api.country;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.ictpro.leteckaposta.leteckaPosta.api.country.capital.Capital;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.UUID;

@Data
@Entity
@Table(name = "countries")
public class Country implements Serializable {

    @Id
    @Column(name = "country_id")
    @EqualsAndHashCode.Exclude
    private UUID id = UUID.randomUUID();

    private String code;

    @OneToMany(mappedBy = "country")
    @ToString.Exclude
    @JsonIgnore
    private Collection<Capital> capitals;

}
