alter table capitals
    ADD COLUMN country_id binary(16) default(null),
    ADD FOREIGN KEY capitals(country_id) REFERENCES countries(country_id) ON DELETE CASCADE;
