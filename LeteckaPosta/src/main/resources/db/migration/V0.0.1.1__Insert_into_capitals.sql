insert into capitals (capital_id, name)
values (uuid_to_bin('08207fb1-71fe-40be-a999-6ecaabba7e0d'), 'Praha'),
       (uuid_to_bin('5afb7cf1-c2f2-4fb8-8c6b-67302073dbe3'), 'Brno'),
       (uuid_to_bin('8e0e5fd8-f8ff-49be-9453-0b0909d471ad'), 'Ostrava');
