SPRING FRAMEWORK A SPRING BOOT - TVORBA BACKENDŮ A MIKROSLUŽEB (SPRB1)
======================================================================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Kurz je určen Java vývojářům, kteří se chtěji seznámit s prací v prostředí Spring Framework.
Součástí školení je mimo seznámení se samotným frameworkem a jeho jednotlivými komponenty také úvod do prostředí Spring Boot, tvorba REST služeb a možnosti napojení aplikací na databáze. 
Dále se seznámíte se základy JPA (Java Persistence API), Hibernate a základy automatizovaného testování v prostředí Spring Boot.

Osnova kurzu 
------------

- **[Úvod do prostředí Spring Boot](/spring/)**
  - Architektura
  - Vytvoření aplikace
  - Spuštění aplikace
  - Podporované jazyky (možnost domluvy - Java či Kotlin, standardně Java)
  - Podporované sestavovací prostředí (možnost domluvy - Gradle či Maven, standardně Maven)
- **[Spring framework - komponenty](/spring.di/)**
  - Dependency injection
  - Druhy komponent a používané anotace
  - Service, Controller, RestController, Repository
  - Scope služeb, singletony
- **[Tvorba REST služeb](/spring.rest)**
  - Úvod do HTTP protokolu a principu REST
  - HTTP metody, status kódy, URI
  - Předávání dat - formát JSON
  - Tvorba REST služeb v Spring
  - Předávání parametrů v query a path
  - Předávání dat pomocí těla HTTP požadavku
  - Generování odpovědi
- **[Přístup k vzdáleným REST službám](/spring.rest.template)**
  - Přehled možností - RestTemplate, WebClient
  - Ukázka práce s WebClient
- **[Práce s databází](/spring.data)**
  - Přehled možností připojení - JDBC, JPA, Spring Data
- **[Úvod do JPA (Java Persistence API) a Hibernate / Spring Data JPA](/spring.data.jpa)**
  - Úvod JPA a Hibernate
  - Modelování entit
  - Základní mapování sloupců, primitivní typy
  - 1:1, 1:N vztahy
  - JPQL - stručný úvod, základní dotazování
  - Spring Data JPA - základní úvod, tvorba repository
- **[Správa transakce v Spring](/spring)**
  - Automatická správa transakce
  - Anotace @Transactional
  - Izolace transakcí
  - Commit, rollback
- **[Automatické testování v prostředí Spring Boot](/spring.test)**
  - Úvod do testování
  - Jednotkové testy
  - Integrační testy pro služby