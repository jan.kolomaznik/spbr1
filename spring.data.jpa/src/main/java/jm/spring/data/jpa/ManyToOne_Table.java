package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.UUID;

public class ManyToOne_Table {

    //tag::entities[]
    @Entity
    public class Person {
        @Id
        private UUID id;

        @ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
        @JoinTable(name="ManyToOne_Table$person_contact",
                joinColumns = @JoinColumn(name="person_id"),
                inverseJoinColumns = @JoinColumn(name="contact_id")
        )
        public Contact author;
        
    }

    @Entity
    public class Contact {
        @Id
        private UUID id;
    }
    //end::entities[]
}
