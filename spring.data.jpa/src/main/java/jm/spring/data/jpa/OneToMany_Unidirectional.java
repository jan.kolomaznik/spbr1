package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.Collection;
import java.util.UUID;

public class OneToMany_Unidirectional {

    //tag::entities[]
    @Entity
    public class Visitor {
        @Id
        private UUID id;

        @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
        @JoinColumn(name="visitor_id")
        public Collection<Ticket> tickets;

    }

    @Entity
    public class Ticket {
        @Id
        private UUID id;
    }
    //end::entities[]
}
