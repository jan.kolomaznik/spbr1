package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.Collection;
import java.util.UUID;

public class ManyToMany_Unidirectional {

    //tag::entities[]
    @Entity
    public class Store {
        @Id
        private UUID id;

        @ManyToMany(cascade = CascadeType.PERSIST)
        public Collection<City> cities;
    }

    @Entity
    public class City {
        @Id
        private UUID id;
    }
    //end::entities[]
}
