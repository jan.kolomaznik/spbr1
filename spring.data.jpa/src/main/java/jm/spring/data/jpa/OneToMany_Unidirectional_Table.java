package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.Collection;
import java.util.UUID;

public class OneToMany_Unidirectional_Table {

    //tag::entities[]
    @Entity
    public class Trainer {
        @Id
        private UUID id;

        @OneToMany
        @JoinTable(
                name = "OneToMany_Unidirectional_Table$TrainedMonkeys",
                joinColumns = @JoinColumn(name = "trainer_id"),
                inverseJoinColumns = @JoinColumn(name = "monkey_id")
        )
        public Collection<Monkey> monkeys;

    }

    @Entity
    public class Monkey {
        @Id
        private UUID id;
    }
    //end::entities[]
}
