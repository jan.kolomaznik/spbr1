package jm.spring.data.jpa;

import javax.persistence.*;
import java.util.Collection;
import java.util.UUID;

public class ManyToMany_Default {

    //tag::entities[]
    @Entity
    public class Store {
        @Id
        private UUID id;

        @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
        public Collection<Product> products;

    }

    @Entity
    public class Product {
        @Id
        private UUID id;

        @ManyToMany(mappedBy="products")
        public Collection<Store> stores;

    }
    //end::entities[]
}
