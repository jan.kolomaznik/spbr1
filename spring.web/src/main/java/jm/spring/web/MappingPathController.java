package jm.spring.web;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;

@Controller
public class MappingPathController {

    // DTO class
    public static class Person {};

    // tag::multiplePaths[]
    @GetMapping(
            value = {"/", "index.html"})
    public String multiplePaths(){
        return "multiplePaths";
    }
    // end::multiplePaths[]

    // tag::pathVariable[]
    @GetMapping("/owners/{ownerId}/pets/{petId}")
    public String pathVariable(
            @PathVariable Long ownerId,
            @PathVariable Long petId,
            Model model) {
        model.addAttribute("ownerId", ownerId);
        model.addAttribute("petId", petId);
        return "pathVariable";
    }
    // end::pathVariable[]

    // tag::prefixSpel[]
    @GetMapping("${api-version}/owners")
    public String prefixSpel() {
        // ...
        return "prefixSpel";
    }
    // end::prefixSpel[]

    // tag::produces[]
    @GetMapping(path = "/persons", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Person> produces() {
        return Collections.EMPTY_LIST;
    }
    // end::produces[]

}
