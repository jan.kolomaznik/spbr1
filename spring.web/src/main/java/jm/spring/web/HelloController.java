package jm.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

// tag::HelloController[]
@Controller // <1>
class HelloController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET) // <2>
    public String handle(Model model) { // <3>
        model.addAttribute("message", "Hello World!");
        return "hello"; // <4>
    }

}
// end::HelloController[]