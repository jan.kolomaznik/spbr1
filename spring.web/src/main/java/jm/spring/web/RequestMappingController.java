package jm.spring.web;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

// tag::RequestMappingController[]
@Controller
@RequestMapping("/persons")
class RequestMappingController {

    // DTO class
    public static class Person {};

    @GetMapping(
            value = "/detail",
            params = "name")
    @ResponseStatus(HttpStatus.CREATED)
    public String getPerson(@RequestParam String name, Model model) {
        // ...
        return "person";
    }

    @PostMapping
    @ResponseBody
    public Person add(@RequestBody Person person) {
        // ...
        return person;
    }
}
// end::RequestMappingController[]
